function dodajPodrzedny() {
    const newElem = document.createElement('div');
    document.getElementById('pierwszy').appendChild(newElem);
}

function usunPodrzedny() {
    const pierwszy = document.getElementById('pierwszy');
    if (pierwszy.hasChildNodes()) {
        pierwszy.removeChild(pierwszy.childNodes[0]);
    }
}
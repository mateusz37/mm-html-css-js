const RED = 'czerwony';

function dodajKlaseCzerwony() {
    document.getElementById('jedyny').classList.add(RED);
}

function usunKlaseCzerwony() {
    document.getElementById('jedyny').classList.remove(RED);
}

function czyElementMaKlaseCzewony() {
    const hasClassCzerwony = document.getElementById('jedyny').classList.contains(RED);
    alert(`Czy element ma klasę ${RED}: ` + (hasClassCzerwony ? 'Tak' : 'Nie'));
}

function usunListenerZDiva() {
    document.getElementById('jedyny').removeEventListener('mousemove', listenerMouseMove)
}

let listenerMouseMove;

window.onload = () => {
    document.getElementById('jedyny').addEventListener('click', () => {
        console.log('Div kliknięty.')
    });

    listenerMouseMove = () => {
        console.log('Mysz porusza się nad divem');
    };

    document.getElementById('jedyny').addEventListener('mousemove', listenerMouseMove);
}